﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Task7
{
  class Program
  {
    static void Main(string[] args)
    {
      var stack = new Stack<MyObj>();

      stack.Push(new MyObj(false));
      stack.Push(new MyObj(false));
      stack.Push(new MyObj(false));
      stack.Push(new MyObj(true));
      stack.Push(new MyObj(false));

      var targetStack = new Stack<MyObj>();

      var sender = new StackSender<MyObj>(stack);

      try
      {
        sender.Execute(targetStack);

        if (sender.WasSuccessfulSend == false)
        {
          Console.WriteLine("Transfer failed on " + sender.Index + " element");
        }

        Program.RepairStackObjects(stack);

        sender.Execute(targetStack, sender.Index);

        Console.WriteLine(sender.WasSuccessfulSend
          ? "Transfer complete"
          : "Transfer failed on " + sender.Index + " element");
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
        Console.WriteLine(ex.StackTrace);
      }
    }

    public static void RepairStackObjects(Stack<MyObj> stack)
    {
      foreach (var failedObj in stack)
      {
        failedObj.Repair();
      }
    }
  }

  public class StackSender<T> where T : IFailedObj
  {
    private Stack<T>.Enumerator _enumerator;

    private readonly Stack<T> _stack;

    public int Index { get; private set; }

    public StackSender(Stack<T> stack)
    {
      _stack = stack;
      _enumerator = _stack.GetEnumerator();
      this.Index = -1;
    }

    public bool Execute(Stack<T> target, int fromIndex = 0)
    {
      this.Index = fromIndex;
      _enumerator = _stack.GetEnumerator();

      for (var i = 0; i < this.Index; ++i)
      {
        _enumerator.MoveNext();
      }

      if (target == null)
      {
        throw new NullReferenceException();
      }

      try
      {
        this.SendStack(target);
      }
      catch (FailedSendException)
      {
        return false;
      }

      return true;
    }

    private void SendStack(Stack<T> newStack)
    {
      while (_enumerator.MoveNext())
      {
        var obj = _enumerator.Current;
        if (obj == null || obj.IsFailed)
        {
          throw new FailedSendException();
        }

        newStack.Push(_enumerator.Current);
        this.Index++;
      }

      _enumerator.Dispose();
      this.Index = -1;
    }

    public bool WasSuccessfulSend
    {
      get
      {
        return this.Index == -1;
      }
    } 
  }

  public class MyObj : IFailedObj
  {
    public bool IsFailed { get; set; }

    public MyObj(bool isFailed)
    {
      this.IsFailed = isFailed;
    }

    public void Repair()
    {
      this.IsFailed = false;
    }
    
  }

  public interface IFailedObj
  {
    bool IsFailed { get; set; }

    void Repair();
  }

  public class FailedSendException : Exception
  {
    public FailedSendException() : base("Send failed")
    {

    }

    public FailedSendException(string message) : base(message)
    {

    }

  }
}
