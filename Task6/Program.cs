﻿using System;
using HelperLib;
using Task5;

namespace Task6
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      while (true)
      {
        Console.WriteLine("Please input line (type exit to end program)");
        var line = Console.ReadLine();

        if (line != null && line.ToLower() == "exit")
        {
          break;
        }

        try
        {
          var result = line.ToInteger();
          Console.Write("Converted to a number: ");
          Console.WriteLine(result);
        }
        catch (EmptyStringException ex)
        {
          Console.WriteLine(ex.Message);
        }
        catch (StringNotNumberException ex)
        {
          Console.WriteLine(ex.Message);
        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
          Console.WriteLine(ex.StackTrace);
        }
      }
    }
  }
}
