﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
  [Title("LINQ Module")]
  [Prefix("Linq")]
  public class LinqSamples : SampleHarness
  {

    private DataSource dataSource = new DataSource();

    [Category("Restriction Operators")]
    [Title("Where - Task 1")]
    [Description("This sample uses the where clause to find all elements of an array with a value less than 5.")]
    public void Linq1()
    {
      int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

      var lowNums =
        from num in numbers
        where num < 5
        select num;

      Console.WriteLine("Numbers < 5:");
      foreach (var x in lowNums)
      {
        Console.WriteLine(x);
      }
    }

    [Category("Restriction Operators")]
    [Title("Where - Task 2")]
    [Description("This sample return return all presented in market products")]
    public void Linq2()
    {
      var products =
        from p in dataSource.Products
        where p.UnitsInStock > 0
        select p;

      foreach (var p in products)
      {
        ObjectDumper.Write(p);
      }
    }

    [Category("Restriction Operators")]
    [Title("Task Linq001")]
    [Description(
      "This sample return a list of all customers whose total turnover (sum of all orders) exceeds a certain value")]
    public void Linq001()
    {
      const decimal x = 10000;

      var result = dataSource.Customers
        .Select(item => new
        {
          item.CustomerID,
          item.CompanyName,
          OrderSum = item.Orders.Sum(order => order.Total)
        }).Where(item => item.OrderSum > x);

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq002A")]
    [Description(
      "This sample return a list of suppliers who are in the same country and the same city for each client")]
    public void Linq002A()
    {
      var result =
        dataSource.Customers.Select(
          customer =>
            new
            {
              customer,
              SuppliersInTheCity =
                dataSource.Suppliers.Where(
                  supplier => supplier.Country == customer.Country && supplier.City == customer.City).ToList()
            })
          .Where(item => item.SuppliersInTheCity.Count > 0).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq002B")]
    [Description(
      "This sample return a list of suppliers who are in the same country and the same city for each client")]
    public void Linq002B()
    {
      //var result =
      //  dataSource.Customers.SelectMany(
      //    customer =>
      //      dataSource.Suppliers.Where(
      //        supplier => supplier.Country == customer.Country && supplier.City == customer.City),
      //    (customer, supplier) => new { customer, supplier }).GroupBy(item => item.customer);

      var result = dataSource.Customers.GroupJoin(dataSource.Suppliers,
        customer => new { customer.City, customer.Country },
        supplier => new { supplier.City, supplier.Country },
        (customer, suppliers) =>
          new { Customer = customer, SuppliersInTheCity = suppliers.ToList() }).Where(item => item.SuppliersInTheCity.Any()).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq003")]
    [Description(
      "This sample return a list of customers who have orders that exceed the sum of the value of X")]
    public void Linq003()
    {
      const decimal x = 5000;

      var result = dataSource.Customers.Where(customer => customer.Orders.Any(order => order.Total > x)).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq004")]
    [Description(
      "This sample return a list of customer list indicating the beginning of a month of the year they became clients")]
    public void Linq004()
    {
      var result =
        dataSource.Customers.Select(
          customer => new { customer.CompanyName, StartDate = customer.Orders.OrderBy(order => order.OrderDate).Select(order => order.OrderDate).FirstOrDefault() }).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq005")]
    [Description(
      "This sample return a list of customer list indicating the beginning of a month of the year they became clients sorted by year, month, turnover")]
    public void Linq005()
    {
      var result = dataSource.Customers.Select(
        customer =>
          new
          {
            customer.CompanyName,
            TurnOver = customer.Orders.Sum(order => order.Total),
            StartDate =
              customer.Orders.OrderBy(order => order.OrderDate).Select(order => order.OrderDate).FirstOrDefault()
          })
        .OrderBy(item => item.StartDate.Year)
        .ThenBy(item => item.StartDate.Month)
        .ThenBy(item => item.TurnOver)
        .ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq006")]
    [Description(
      "The customers who have specified a non-digital code or full region or in the phone area code Unknown")]
    public void Linq006()
    {
      int outValue;

      var result =
        dataSource.Customers.Where(
          item =>
            int.TryParse(item.PostalCode, out outValue) == false || string.IsNullOrEmpty(item.Region) ||
            item.Phone.Trim()[0] == '(').ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq007")]
    [Description(
      "Group all products by category, in - on the availability of stock within the last group of sort by cost")]
    public void Linq007()
    {
      var result =
        dataSource.Products.GroupBy(item => item.Category)
        .Select(item => new { item.Key, GroupItems = item.GroupBy(l => l.UnitsInStock > 0).Select(g => new { g.Key, GroupItems = g.OrderBy(h => h.UnitPrice).ToList() }).ToList() }).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq008")]
    [Description(
      "Group the items by groups of \"cheap\", \"average price\", \"expensive\"")]
    public void Linq008()
    {
      var firstVal = 40M;
      var secondVal = 100M;

      var result =
        dataSource.Products.Select(
          product =>
            new
            {
              product,
              PriceCategory =
                product.UnitPrice < firstVal ? "cheap" : product.UnitPrice < secondVal ? "average price" : "expensive"
            })
          .GroupBy(item => item.PriceCategory)
          .Select(item => new {item.Key, GroupOfProducts = item.Select(g => g.product).ToList()}).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq009")]
    [Description(
      "Calculate the average profitability of each city")]
    public void Linq009()
    {
      var result =
        dataSource.Customers.GroupBy(customer => customer.City)
          .Select(
            item =>
              new
              {
                item.Key,
                AverageOrder = item.Where(o => o.Orders.Any()).Average(g => g.Orders.Average(h => h.Total)),
                AverageIntensity = item.Average(g => g.Orders.Count())
              }).ToList();

      this.WriteResult(result);
    }

    [Category("Restriction Operators")]
    [Title("Task Linq010")]
    [Description(
      "Make an annual average customer activity by month, by year statistics for the years and months")]
    public void Linq010()
    {
      var monthResult =
        dataSource.Customers.SelectMany(customer => customer.Orders,
          (customer, order) => new {order.OrderDate.Month, order.Total})
          .GroupBy(item => item.Month)
          .Select(item => new {Month = item.Key, AverageOrderTotal = item.Average(g => g.Total)}).OrderBy(item => item.Month).ToList();

      this.WriteResult(monthResult);

      var yearResult =
        dataSource.Customers.SelectMany(customer => customer.Orders,
          (customer, order) => new { order.OrderDate.Year, order.Total })
          .GroupBy(item => item.Year)
          .Select(item => new { Year = item.Key, AverageOrderTotal = item.Average(g => g.Total) }).OrderBy(item => item.Year).ToList();

      this.WriteResult(yearResult);

      var monthAndYearResult =
        dataSource.Customers.SelectMany(customer => customer.Orders,
          (customer, order) => new { MonthAndYear = new {order.OrderDate.Month, order.OrderDate.Year}, order.Total })
          .GroupBy(item => item.MonthAndYear)
          .Select(item => new { item.Key.Month, item.Key.Year, AverageOrderTotal = item.Average(g => g.Total) }).OrderBy(item => item.Year).ThenBy(item => item.Month).ToList();

      this.WriteResult(monthAndYearResult);
    }


    private void WriteResult(IEnumerable objects)
    {
      foreach (var p in objects)
      {
        ObjectDumper.Write(p);
      }
    }
  }
}
