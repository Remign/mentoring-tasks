﻿namespace Task3
{
  public interface IFactory<out T> where T: new()
  {
    T CreateInstance();
  }

  public class Factory<T> : IFactory<T> where T : new()
  {
    public T CreateInstance()
    {
      return new T();
    }
  }
}