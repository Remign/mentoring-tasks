﻿using System;

namespace Task3
{
  class Program
  {
    static void Main(string[] args)
    {
      //var factory = new Factory<Person>();
      //var p1 = factory.CreateInstance();
      //p1.FirstName = "Alex";
      //p1.LastName = "Ivanov";

      //var p2 = factory.CreateInstance();
      //p2.FirstName = "Alex";
      //p2.LastName = "Ivanov";

      var p1 = new Person
      {
        FirstName = "Alex",
        LastName = "Ivanov"
      };

      var p2 = new Person
      {
        FirstName = "Alex",
        LastName = "Ivanov"
      };

      Console.WriteLine(p1.Equals(p2)); // true
    }
  }

  public class Person
  {
    protected bool Equals(Person other)
    {
      return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName);
    }

    public override int GetHashCode()
    {
      unchecked
      {
        return ((FirstName != null ? FirstName.GetHashCode() : 0) * 397) ^ (LastName != null ? LastName.GetHashCode() : 0);
      }
    }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      return obj.GetType() == this.GetType() && Equals((Person)obj);
    }
  }
}
