﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11Reflection3
{
  class Program
  {
    static void Main(string[] args)
    {
      var dictionary = CreateDictionary(typeof(Human), typeof(Person));
      ObjectDumper.Write(dictionary);
    }

    static object CreateDictionary(Type keyType, Type valueType)
    {
      var dictionaryType = typeof (Dictionary<,>);
      var genericDictionaryType = dictionaryType.MakeGenericType(keyType, valueType);

      var dictionary = Activator.CreateInstance(genericDictionaryType);

      var keyTypeConstructor = keyType.GetConstructor(Type.EmptyTypes);
      var valueTypeConstructor = valueType.GetConstructor(Type.EmptyTypes);

      if (keyTypeConstructor != null && valueTypeConstructor != null)
      {
        var dictionaryAddMethod = genericDictionaryType.GetMethod("Add");

        for (int i = 0; i < 10; ++i)
        {
          var key = Activator.CreateInstance(keyType);
          var value = Activator.CreateInstance(valueType);

          dictionaryAddMethod.Invoke(dictionary, new[]{key, value});
        }
      }

      return dictionary;
    }
  }

  class Human
  {
    public string Name { get; set; }

    public int Age { get; set; }

  }

  class Person
  {
    public string Name { get; set; }

    public int Age { get; set; }

    public Person()
    {
    }

    public Person(string name, int age)
    {
      this.Name = name;
      this.Age = age;
    }
  }
}
