﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task5;

namespace HelperLib
{
  public static class StringParser
  {
    public static int ToInteger(this string str)
    {
      if (string.IsNullOrEmpty(str))
      {
        throw new EmptyStringException();
      }

      var charArr = str.ToCharArray();

      var intArr = charArr.Select(item => item - 48).ToArray();

      if (intArr.Any(item => item >= 10))
      {
        throw new StringNotNumberException("This string is not a number");
      }

      var result = intArr.Select((t, i) => t * (int)Math.Pow(10, intArr.Length - 1 - i)).Sum();

      return result;
    }
  }

  public class StringNotNumberException : Exception
  {
    public StringNotNumberException() : base("String not a number")
    {
    }

    public StringNotNumberException(string message) : base(message)
    {
    }
  }
}
