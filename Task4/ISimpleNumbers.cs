﻿using System.Collections.Generic;
using System.Linq;

namespace Task4
{
  public interface ISimpleNumbers
  {
    List<int> GetSimpleNumbers(int size);
  }

  public class SimpleNumbers : ITaskExecutor<List<int>, int>, ISimpleNumbers
  {
    public List<int> Execute(int parameter)
    {
      return this.GetSimpleNumbers(parameter);
    }

    public List<int> GetSimpleNumbers(int size)
    {
      var index = 0;
      var primeNums = new int[size];

      var k = 1;
      var primes = 0;

      while (primes < size)
      {
        var count = 0;
        for (var i = 1; i <= k; ++i)
        {
          if (k % i == 0)
          {
            count++;
          }
        }
        if (count == 2)
        {
          primeNums[index++] = k;
          primes++;
        }
        k++;
      }

      return primeNums.ToList();
    }
  }

}