﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task4
{
  class Program
  {
    static void Main(string[] args)
    {
      //var simpleNumbersExecutor = new SimpleNumbers();
      //var sumAndFloorExecutor = new SumAndFloor(simpleNumbersExecutor);
      //var executor = new TaskExecutor<int, int>(sumAndFloorExecutor);

      //var result = executor.Execute(4);


      var result = DoNecessaryCounts(4);

      Console.WriteLine(result);

    }

    public static int DoNecessaryCounts(int size)
    {
      var list = SimpleNumbers(size);

      var sum = list.Select((t, i) => (double)t / (i + 1)).Sum();

      var result = (int)Math.Floor(sum);

      return result;
    }

    public static List<int> SimpleNumbers(int size)
    {
      #region modifiedErastofen(not work)

      //var aim = size;

      //var startSize = aim;
      //var addSize = aim;

      //var nums = new bool[startSize];

      //var primeNums = new int[aim];

      //var foundPrimes = 0;

      //for (var i = 2; i < nums.Length; i++)
      //  nums[i] = true;

      //var addition = false;

      //while (true)
      //{
      //  if (addition)
      //  {
      //    Array.Resize(ref nums, nums.Length + addSize);

      //    for (var i = 0; i < foundPrimes; i++)
      //    {
      //      var curNum = primeNums[i];
      //      if ((addSize + ((nums.Length - addSize) % curNum)) < curNum)
      //        continue;
      //      for (var j = ((nums.Length - addSize) / curNum) * curNum; j < nums.Length; j += curNum)
      //        nums[j] = false;
      //    }
      //  }
      //  else
      //    addition = true;

      //  int iter;
      //  if (foundPrimes == 0)
      //    iter = 2;
      //  else
      //    iter = primeNums[foundPrimes - 1] + 2;

      //  for (; iter < nums.Length; iter++)
      //  {
      //    if (nums[iter])
      //    {
      //      primeNums[foundPrimes] = iter;
      //      foundPrimes++;
      //      if (foundPrimes == aim)
      //        break;

      //      for (var j = iter + iter; j < nums.Length; j += iter)
      //        nums[j] = false;
      //    }
      //  }
      //  if (foundPrimes == aim)
      //    break;
      //}
      #endregion

      var index = 0;
      var primeNums = new int[size];

      var k = 1;
      var primes = 0;

      while (primes < size)
      {
        var count = 0;
        for (var i = 1; i <= k; ++i)
        {
          if (k % i == 0)
          {
            count++;
          }
        }
        if (count == 2)
        {
          primeNums[index++] = k;
          primes++;
        }
        k++;
      }

      return primeNums.ToList();
    }
  }
}
