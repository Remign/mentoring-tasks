﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task4
{
  public interface ITaskExecutor<out T, in TV>
  {
    T Execute(TV parameter);
  }

  public class TaskExecutor<T, TV> : ITaskExecutor<T, TV>
  {
    private readonly ITaskExecutor<T, TV> _logic;

    public TaskExecutor(ITaskExecutor<T, TV> logic)
    {
      _logic = logic;
    }

    public T Execute(TV parameter)
    {
      return _logic.Execute(parameter);
    }
  }

  public class SumAndFloor : ITaskExecutor<int, int>
  {
    private readonly ITaskExecutor<List<int>, int> _logic;

    public SumAndFloor(ITaskExecutor<List<int>, int> logic)
    {
      _logic = logic;
    }

    public int Execute(int parameter)
    {
      return this.DoNecessaryCounts(parameter);
    }

    public int DoNecessaryCounts(int size)
    {
      var list = _logic.Execute(size);

      var sum = list.Select((t, i) => (double)t / (i + 1)).Sum();

      var result = (int)Math.Floor(sum);

      return result;
    }
  }
}