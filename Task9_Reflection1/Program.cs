﻿using System;
using System.Collections.Generic;

namespace Task9_Reflection1
{
  class Program
  {
    static void Main(string[] args)
    {
      var list = CreateCustomList(typeof(Person));

      ObjectDumper.Write(list);

      var secondlist = CreateCustomList(typeof(int));

      ObjectDumper.Write(secondlist);
    }

    static object CreateCustomList(Type type)
    {
      var listType = typeof (List<>);
      var genericListType = listType.MakeGenericType(type);
      var list = Activator.CreateInstance(genericListType);

      var typeConstructor = type.GetConstructor(Type.EmptyTypes);

      if (typeConstructor != null || type.IsValueType)
      {
        var listAddMethod = genericListType.GetMethod("Add");

        for (var i = 0; i < 5; ++i)
        {
          var newInstance = Activator.CreateInstance(type);

          listAddMethod.Invoke(list, new[] { newInstance });
        }
      }

      return list;
    }
  }


  class Person
  {
    public string Name { get; set; }

    public int Age { get; set; }

    public Person()
    {
    }

    public Person(string name, int age)
    {
      this.Name = name;
      this.Age = age;
    }
  }
}
