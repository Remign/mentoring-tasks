﻿using System;

namespace Task5
{
  public class Program
  {
    private static void Main(string[] args)
    {
      while (true)
      {
        Console.WriteLine("Please input line (type exit to end program)");
        var line = Console.ReadLine();

        if (line != null && line.ToLower() == "exit")
        {
          break;
        }

        try
        {
          Console.WriteLine(Program.GetFirstLetter(line));
        }
        catch (EmptyStringException exception)
        {
          Console.Write("First char: ");
          Console.WriteLine(exception.Message);
        }
        catch (Exception ex)
        {
          Console.WriteLine("Some error occured");
          Console.WriteLine(ex.StackTrace);
        }
      }

    }

    public static char GetFirstLetter(string line)
    {
      if (line == string.Empty)
      {
        throw new EmptyStringException("The line is empty");
      }

      return line[0];
    }

  }

  public class EmptyStringException : Exception
  {
    public EmptyStringException(): base("The line is empty")
    {
    }

    public EmptyStringException(string message) : base(message)
    {
    }
  }
}
