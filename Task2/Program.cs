﻿using System;

namespace Task2
{
  class Program
  {
    static void Main(string[] args)
    {
      var a = new Product
      {
        Name = "Milk",
        Price = 20,
        UnitsNumber = 2
      };

      var b = new Product
      {
        Name = "Milk",
        Price = 20,
        UnitsNumber = 2
      };

      //var result = a.Equals(b);

      var result = b.GetHashCode();

      Console.WriteLine(result);
    }
  }

  struct Product
  {
    public string Name { get; set; }

    public decimal Price { get; set; }

    public int UnitsNumber { get; set; }

    public override int GetHashCode()
    {
      return (int)Price ^ UnitsNumber;
    }

    public override bool Equals(object obj)
    {
      if ((obj is Product) == false)
      {
        return false;
      }

      var item = (Product) obj;

      return this.Name == item.Name && this.Price == item.Price && this.UnitsNumber == item.UnitsNumber;
    }
  }
}
