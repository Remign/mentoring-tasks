﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
  class Program
  {
    static void Main(string[] args)
    {
      var data = new List<InfoData>
      {
        new InfoData
        {
          FirstName = "a",
          LastName = "b"
        },
        new InfoData
        {
          FirstName = "d",
          LastName = "c"
        }
      };

      var source = new Source();

      source.CheckAndProceed(data);

      foreach (var infoData in data)
      {
        Console.WriteLine("{0} {1}", infoData.FirstName, infoData.LastName);
      }
    }
  }

  interface IInfoData
  {
    string FirstName { get; set; }
    string LastName { get; set; }

    void DoSmth();
  }

  struct InfoData : IInfoData
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public void DoSmth()
    {
      this.FirstName = "qwe";
    }

    public override int GetHashCode()
    {
      var charArray = (this.FirstName + this.LastName).ToCharArray();

      var result = 0;

      unchecked
      {
        result = charArray.Aggregate(result, (current, t) => current + t);
      }

      return result;
    }

    public override bool Equals(object obj)
    {
      if ((obj is InfoData) == false)
      {
        return false;
      }

      var item = (InfoData) obj;

      return this.FirstName == item.FirstName && this.LastName == item.LastName;
    }
  }

  class Source
  {
    internal void CheckAndProceed(IEnumerable<InfoData> data)
    {
      var dest = new Destination();

      //do something

      var iData = data.Select(p => (IInfoData)p).ToList();

      dest.ProceedData(iData);
    }
  }

  class Destination
  {
    internal void ProceedData(IEnumerable<IInfoData> data)
    {
      foreach (var item in data)
      {
        item.DoSmth();
        Console.WriteLine("{0} {1}", item.FirstName, item.LastName);
      }
    }
  }
}
